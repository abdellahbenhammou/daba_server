#--------------------------------------------------------------------------
__author__ = 'Abdellah'#***************************************************
#--------------------------------------------------------------------------

#***********************defining variables needed

data = []
results = []
i = 0
prefix_len = 0
current_lowest = -1
current_operator = 'None'
current_prefix = 'None'
#--------------------------------------------------------------------------
#***********************getting the user input and starting the processing

print 'Welcome! There are two options, enter 1 for my values or 2 to give your file containing the values separated by space(prefix cost operator)'
if raw_input(' 1 or 2 ?: ') == '1':#if user choses 1, I get the pre defined data
    prefixes = ['1', '268', '46', '4620', '468', '4631', '4673', '46732', '1', '3', '46', '467', '48', '46721', '6']
    cost = [0.9, 5.1, 0.17, 0.0, 0.15, 0.15, 0.9, 1.1, 0.92, 0.5, 0.2, 1.0, 1.2, 1.4, 1.5]
    operator = ['A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'B', 'B', 'B', 'B', 'B', 'B', 'C']
#***********************filling the list of dicts
    for p in prefixes:
        data.append({'prefix': p, 'cost': cost[i], 'operator': operator[i]})
        i = i + 1
#***********************if user choses 2, I get the data from the file, assuming format is respected
else:
    filename = raw_input('enter file name(must be within the same directory as the program):')
#***********************trying to read the file
    try:
        fp = open( filename, "r" )
    except:
        print 'file not found'
    for line in fp:#filling the list of dicts
        data.append({'prefix': line.split()[0], 'cost': line.split()[1], 'operator': line.split()[2]})
    fp.close()

#--------------------------------------------------------------------------
#***********************sorting the list on the prefix's length(reversed)
data = sorted(data, key=lambda d: len(d['prefix']), reverse=True)
#--------------------------------------------------------------------------

number = raw_input('please enter a phone number to check for: ')#the phone number to test with
#***********************getting the cheapest cost
for item in data:
#***********************checking if the phone number actually contains the prefix
    if number.startswith(item.get('prefix')):
#***********************checking if the current prefix length is lower than the current prefixe's length
        if prefix_len <= len(item.get('prefix')):
#***********************checking the current lowest cost to update it if need be
            if current_lowest == -1 or current_lowest >= item.get('cost'):
#***********************updating values as necessary
                prefix_len = len(item.get('prefix'))
                current_lowest = item.get('cost')
                current_operator = item.get('operator')
                current_prefix = item.get('prefix')
        else:
            break
#***********************printing the results
print 'results: prefix: ' + current_prefix + ' ** cost: ' + str(current_lowest) + ' ** operator: ' + current_operator