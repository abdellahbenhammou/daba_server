from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
     url(r'^$', 'Daba.views.list_', name='home'),
     url(r'^list/$', 'Daba.views.list', name='list'),
     url(r'^login/$', 'Daba.views.login', name='login'),
     url(r'^register/$', 'Daba.views.register', name='register'),
     url(r'^feed/$', 'Daba.views.userFeed', name='feed'),
     url(r'^ufollow/$', 'Daba.views.followUser', name='ufollow'),
     url(r'^lfollow/$', 'Daba.views.followLocation', name='lfollow'),
     url(r'^uunfollow/$', 'Daba.views.unFollowUser', name='uunfollow'),
     url(r'^lfeed/$', 'Daba.views.locationFeedByUser', name='lfeed'),
     url(r'^myvideos/$', 'Daba.views.oneUserFeed', name='oneuserfeed'),

    # url(r'^Daba_Server/', include('Daba_Server.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
