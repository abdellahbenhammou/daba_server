# Create your views here.
# -*- coding: utf-8 -*-
from plistlib import Data
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt
from Daba_Server.forms import DocumentForm
from Daba.models import *
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib import auth
import re, requests, json
from django.core import serializers
from django.db import transaction
from itertools import chain
from easy_thumbnails_ffmpeg import source_generators
from Daba_Server import settings
from PIL import Image
from django.db.models import Q


@transaction.commit_on_success
@csrf_exempt
def list_(request):
    # Handle file upload
    if request.user.is_authenticated or request.method == 'POST':
        if request.method == 'POST':
            title = request.POST.get('title')
            lat = request.POST.get('lat')
            lng = request.POST.get('lng')
            video = request.FILES['docfile']
            thumb = request.FILES['thumb']
            id = request.POST.get('id')
            datetime = request.POST.get('datetime')
            print 'the user id: ' + str(id) + 'datetime: ' + datetime


            """Adding the new video with the thumbnail"""
            newdoc = Video.objects.create(docfile = video, thumbnail = thumb, title=title, user_id = str(id), timeOfUpload = datetime, up = 0, down = 0, views = 0, report = 0)
            #print 'the video path: ' + settings.MEDIA_ROOT + '\\' + str(newdoc.docfile).replace('/', '\\')
            print 'video is here: ' + str(newdoc.title)



            """Inserting the tags"""
            insertTags(newdoc, title)

            """getting the foursquare data"""
            result = requests.get("https://api.foursquare.com/v2/venues/search?client_id=RBDDT4EYVRG4EQYYHEH3L03QITTLJNTNE4RJG5BDW1KA2553"
                                  "&client_secret=OS3ETBY33JVVCGASEJT52LJYSX3C1YQFZV2EIZ1JSTGEHGZI&ll="+lat+","+lng+"&v=20140317&limit=5",
                                  headers = {"Accept-Language": "en-US"})

            print 'status: ' + str(result.status_code)
            #print 'foursquare: ' + result.json()
            print 'foursquare text: ' + result.text
            a = result.json()
            """"checking if the location already exists, if yes, bind the video to it using the location video
            table, if no, the location is added and then bound to the video using the location video table"""
            for i in range(len(a['response']['venues'])):
                checkLocation = LocationMetaData.objects.filter(name = a['response']['venues'][i]['name'],
                                                                address = a['response']['venues'][i]['location'].get('address', ''),
                                                                city = a['response']['venues'][i]['location'].get('city', ''),
                                                                country = a['response']['venues'][i]['location'].get('country', ''),
                                                                street = a['response']['venues'][i]['location'].get('street', ''))
                if not checkLocation.exists():
                    new_location = LocationMetaData.objects.create(name = a['response']['venues'][i]['name'],
                                                                   address = a['response']['venues'][i]['location'].get('address', ''),
                                                                   city = a['response']['venues'][i]['location'].get('city', ''),
                                                                   country = a['response']['venues'][i]['location'].get('country', ''),
                                                                   street = a['response']['venues'][i]['location'].get('street', ''),
                                                                   longitude = lng,
                                                                   latitude = lat)
                    LocationVideo.objects.create(video = newdoc, location = new_location )
                else:

                    LocationVideo.objects.create(video = newdoc, location = checkLocation[0])

            """here"""

            print 'here: ' + a['response']['venues'][0]['name']

            return HttpResponse("OK")
        else:
            return HttpResponse("GET used")
    else:
        return HttpResponse("GET used")

@csrf_exempt
def register(request):
    if request.method == 'POST':
        firstName = request.POST.get('firstName')
        lastName = request.POST.get('lastName')
        email = request.POST.get('email')
        username = request.POST.get('username')
        password = request.POST.get('password')
        birthdate = request.POST.get('birthdate')
        city = request.POST.get('city')
        country = request.POST.get('country')

        user = User.objects.create_user(first_name = firstName, last_name = lastName, email = email, password = password, username = username)
        user.save()
        user_id = user.id
        if user_id is not None:
            profile = Profile(user_id = user_id, first_name = firstName, last_name = lastName, username = username, birthDate = birthdate, city = city, country = country, points = 0, followers = 0)
            profile.save()
            profileID = profile.id
            return HttpResponse("success: " + str(profileID) + " and: " + str(user_id))
        return
    else:
        return HttpResponse("GET used")

@csrf_exempt
def login(request):
    if request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        print 'username: ' + username
        print 'password: ' + password
        user = authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            print '1'
            loginDict = {}
            loginDict['user_id'] = user.id.__str__()
            loginDict['username'] = user.username.__str__()
            loginDict['loginStatus'] = 'AUTHENTICATED'
            loginDict['firstname'] = user.first_name.__str__()
            loginDict['lastname'] = user.last_name.__str__()

            return  HttpResponse(loginDict.__str__())
        else:
            print '2'
            loginDict = {}
            loginDict['loginStatus'] = 'INCORRECT CREDS'
            return HttpResponse(loginDict.__str__())
    else:
        print '3'
        return HttpResponse("GET used")


def insertTags(video, title):
    l = []
    l = re.findall(r"#(\w+)", title)
    tags = ""
    for i in l:
        print 'tag: ' + str(i)
        tag = Tags.objects.filter(tagTitle = str(i))
        print 'counter of tags: ' + str(tag.exists())
        if not tag.exists():
            new_tag = Tags.objects.create(tagTitle = str(i))
            new_asc = TagsCross.objects.create(tag = new_tag, video = video)
            print 'created the tag: ' + str(new_tag) + ' and asc: ' + str(new_asc)

        else:
            existing_tag = Tags.objects.filter(tagTitle = str(i))
            #existing_tag = Tags(existing_tag)
            new_asc = TagsCross.objects.create(tag = existing_tag[0], video = video)
            print 'created: ' + str(new_asc)

@csrf_exempt
def userFeed(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            data = request.body
            #print 'the client json: ' + str(request.body)
            feedResponse = {}

            videos = Video.objects.filter(user__in = UserFollowers.objects.filter(follower_id=User.objects.get(id=id)).values('user_id'))
            feedResponse['videos'] = serializers.serialize('json', videos, fields=('docfile', 'title'))

            print str(serializers.serialize('json', videos, fields=('docfile', 'title')))
            return HttpResponse(feedResponse.get('videos'))
        else:
            return HttpResponse("GET used")

    else:
        return HttpResponse("Not authenticated")

@csrf_exempt
def followUser(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            toFollow = request.POST.get('id')
            toFollowUserID = Profile.objects.filter(user=toFollow).values('user_id')
            userToFollow = User.objects.get(id = toFollowUserID)
            theFollow = UserFollowers.objects.create(user = userToFollow, follower = request.user)
            return HttpResponse("OK")
        else:
            return HttpResponse("GET used")
    else:
        return HttpResponse("Not authenticated")

@csrf_exempt
def unFollowUser(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            toUnFollow = request.POST.get('id')
            toUnFollowUserID = Profile.objects.filter(user=toUnFollow).values('user_id')
            userToUnFollow = User.objects.get(id = toUnFollowUserID)
            UserFollowers.objects.filter(user = userToUnFollow, follower = request.user).delete()
            return HttpResponse("OK")
        else:
            return HttpResponse("GET used")
    else:
        return HttpResponse("Not authenticated")

@csrf_exempt
def followLocation(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            lat = request.POST.get('lat')
            lng = request.POST.get('lng')
            id = request.POST.get('id')
            result = requests.get("https://api.foursquare.com/v2/venues/search?client_id=RBDDT4EYVRG4EQYYHEH3L03QITTLJNTNE4RJG5BDW1KA2553"
                                  "&client_secret=OS3ETBY33JVVCGASEJT52LJYSX3C1YQFZV2EIZ1JSTGEHGZI&ll="+lat+","+lng+"&v=20140317&limit=5",
                                  headers = {"Accept-Language": "en-US"})
            print 'lat: ' + lat + ' -- lng: ' + lng
            print 'status: ' + str(result.status_code)
            #print 'foursquare: ' + result.json()
            print 'foursquare text: ' + result.text
            a = result.json()
            """"checking if the location already exists, if yes, bind the user to it using the locationFollowers
            table, if no, the location is added and then bound to the user using the locationFollowers table"""
            for i in range(len(a['response']['venues'])):
                checkLocation = LocationMetaData.objects.filter(name = a['response']['venues'][i]['name'],
                                                                address = a['response']['venues'][i]['location'].get('address', ''),
                                                                city = a['response']['venues'][i]['location'].get('city', ''),
                                                                country = a['response']['venues'][i]['location'].get('country', ''),
                                                                street = a['response']['venues'][i]['location'].get('street', ''))
                if not checkLocation.exists():
                    new_location = LocationMetaData.objects.create(name = a['response']['venues'][i]['name'],
                                                                   address = a['response']['venues'][i]['location'].get('address', ''),
                                                                   city = a['response']['venues'][i]['location'].get('city', ''),
                                                                   country = a['response']['venues'][i]['location'].get('country', ''),
                                                                   street = a['response']['venues'][i]['location'].get('street', ''),
                                                                   longitude = lng,
                                                                   latitude = lat)
                    LocationFollowers.objects.create(location = new_location, follower = User.objects.get(id=id))
                else:
                    #LocationVideo.objects.create(location = checkLocation, follower = request.user )
                    LocationFollowers.objects.create(location = checkLocation[0], follower = User.objects.get(id = id))

            return HttpResponse({'status':'ok'}.__str__())
        else:
            return HttpResponse("GET used")
    else:
        return HttpResponse("Not authenticated")


@csrf_exempt
def locationFeedByUser(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            id = request.POST.get('id')
            
            videos  = Video.objects.filter(Q(user = id) | Q(id__in= LocationVideo.objects.filter(
                location__in = LocationMetaData.objects.filter(
                    id__in = LocationFollowers.objects.filter(
                        follower = id).values('location_id')).values('id')).values('video_id'))).order_by('-timeOfUpload')[:10]
            feedResponse = {'videos': '',
                            'locations': '',}
            #videos_json = serializers.serialize('json', videos, fields=('docfile', 'title', 'up,', 'down', 'views'))
            ids = []
            video_ids = []
            ids = videos.all().values('id')

            for l in ids:
                print l.get('id')
                video_ids.append(l.get('id'))

            u_ids = []
            user_ids = []
            u_ids = videos.all().values('user_id')
            #print 'the video: ' + str(videos[0].user.id)
            for l in u_ids:
                print l.get('user_id')
                user_ids.append(l.get('user_id'))


            locations = LocationMetaData.objects.filter(id__in = LocationVideo.objects.filter(video__in = video_ids).values('location_id'))
            #locations_json = serializers.serialize('json', locations, fields=('longitude', 'latitude', 'name,', 'address', 'city', 'country'))
            video_location = LocationVideo.objects.filter(video__in = video_ids)
            video_profiles = Profile.objects.filter(user__id__in = user_ids)
            #video_users = User.objects.filter(id__in = user_ids)


            result_list = list(chain(videos, locations, video_location, video_profiles))
            result_json = serializers.serialize('json', result_list)

            return HttpResponse(result_json)
        else:
            return HttpResponse("GET used")
    else:
        return HttpResponse("Not authenticated")


@csrf_exempt
def oneUserFeed(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            id = request.POST.get('id')

            videos  = Video.objects.filter(Q(user = id))
            ids = []
            video_ids = []
            ids = videos.all().values('id')

            for l in ids:
                print l.get('id')
                video_ids.append(l.get('id'))

            locations = LocationMetaData.objects.filter(id__in = LocationVideo.objects.filter(video__in = video_ids).values('location_id'))
            #locations_json = serializers.serialize('json', locations, fields=('longitude', 'latitude', 'name,', 'address', 'city', 'country'))
            video_location = LocationVideo.objects.filter(video__in = video_ids)
            result_list = list(chain(videos, locations, video_location))
            result_json = serializers.serialize('json', result_list)

            return HttpResponse(result_json)
        else:
            return HttpResponse("GET used")
    else:
        return HttpResponse("Not authenticated")


@csrf_exempt
def unFollowLocation(request):
    pass
