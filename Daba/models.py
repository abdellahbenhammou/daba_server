# -*- coding: utf-8 -*-
from collections import defaultdict
from django.db import models
from django.contrib.auth.models import User



class Profile(models.Model):
    user = models.ForeignKey(User)
    username = models.TextField()
    first_name = models.TextField()
    last_name = models.TextField()
    birthDate = models.DateField()
    city = models.TextField()
    country = models.TextField()
    points = models.IntegerField()
    followers = models.IntegerField(default=0)

    

class Video(models.Model):
    user = models.ForeignKey(User)
    docfile = models.FileField(upload_to='videos')
    thumbnail = models.FileField(upload_to='thumbnails')
    title  = models.TextField()
    up = models.IntegerField(default=0)
    down = models.IntegerField(default=0)
    report = models.IntegerField(default=0)
    views = models.IntegerField(default=0)
    timeOfUpload = models.DateTimeField()

class Tags(models.Model):
    tagTitle = models.TextField()

class TagsCross(models.Model):
    tag = models.ForeignKey(Tags)
    video = models.ForeignKey(Video)

class UserFollowers(models.Model):
    user = models.ForeignKey(User, related_name='profileID')
    follower = models.ForeignKey(User, related_name='followerID')

class LocationMetaData(models.Model):
    longitude = models.FloatField()
    latitude = models.FloatField()
    name = models.TextField()
    address = models.TextField()
    city = models.TextField()
    country = models.TextField()
    street = models.TextField()

class LocationFollowers(models.Model):
    location = models.ForeignKey(LocationMetaData)
    follower = models.ForeignKey(User)


class LocationVideo(models.Model):
    video = models.ForeignKey(Video)
    location = models.ForeignKey(LocationMetaData)
    class Meta:
        unique_together = (("video", "location"),)

